#include <stdio.h>

int main()
{
    int c;

    c = getchar() != EOF;
    printf("The value of C is %1d\n", c);
    printf("The value of EOF is %d\n", EOF);

    return 0;
}
#include <stdio.h>
/*
Word counting program.
*/
#define IN 1
#define OUT 0

int main()
{
    /*State denote whether we scanning a word or not.*/
    int state, nl, nw, nc, c;
    nl = nw = nc = 0;

    /*Currently, we are not scanning any word.*/
    state = OUT;
    while ((c = getchar()) != EOF)
    {
        if (c == '\n')
            ++nl;
        else if (c == ' ' || c == '\n' || c == '\t')
            state = OUT;
        /*If we scanned a character, state needs to be changed to IN*/
        else if (state == OUT)
        {
            state = IN;
            ++nw;
        }
    }

    printf("Total words: %d\n", nw);
    return 0;
}
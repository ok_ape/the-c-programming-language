# Loops

Loops help us iterate over a set of values for a true condition in C.

## For Loop

```c
main()
{
    int a;
    for(a = 1; a <= 100; ++a)
        printf("%d\n", a);
}
```

## While Loop

```c
main()
{
    int a;
    while((a = getchar()) != 'x')
        printf("%c\n", a);
}
```

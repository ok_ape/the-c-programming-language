# Functions

**Definition**: A set of procedure, that can be called anytime in main().
Usage: Declare function at the top and define it after `main()`.
On the top of the code we have declared out `sum` function which is also called **_function prototype_**.

```c
int sum(int a, int b);
int main()
{
    printf("Sum of 2 and 3 is %d.\n", sum(2,3));
    return 0;
}
int sum(int a, int b)
{
    return a + b;
}
```

**Function Call by Value**: As we know that the parameters passed in a functions are denoted by names, these are temporary variables that store a value, which can be modified in function routine. Hence any change to those temporary variables will not change the value of arguments passed. This method of dealing with variables through their values is called _call by value_. You can also pass address in the function call and that will permanently change the value of the argument passed.

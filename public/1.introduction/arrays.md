# Arrays

Arrays in C are data structures that store specified units of one datatypes.

```c
int i;
int myArrayOfInts[10];

for (i = 0; i < 10; ++i)
    myArrayOfInts[i] = i;
```
